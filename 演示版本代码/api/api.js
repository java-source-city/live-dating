/* //配置后端地址
export default {
	//baseUrl: "http://103.46.128.45:15874/gy/forum"//外网测试地址
	//baseUrl: "http://127.0.0.1:9001/gy/forum"
} */

//const baseUrl = "http://103.46.128.45:15874/gy/forum"
const baseUrl = "http://127.0.0.1:9001/gy/forum"
//const baseUrl = "http://129.204.169.240:9001/gy/forum"
//带Token请求
export const request = (opts) => {
	//此token是登录成功后后台返回保存在storage中的
	const userInfo = uni.getStorageSync('userInfo');    //获取token
	const token = userInfo.token  //获取token
	
/* 	let token = "";
	uni.getStorage({
		key: 'token',
		success: function(ress) {
			token = ress.data
		}
	}); */
	let httpDefaultOpts = {
		url: baseUrl + opts.url,
		method: opts.method,
		data:opts.data,
		header: {
			'Token': token,
			},
		dataType: 'json',
	}
	let promise = new Promise(function(resolve, reject) {
		uni.request(httpDefaultOpts).then(
			(res) => {
				resolve(res[1])
				
				console.log("res1==="+res[1].statusCode);
				let code = res[1].statusCode;
				console.log(code)
				
				if(code == 401){
					uni.showModal({
						title: '提示',
						content: "授权过期,请重新登录",
						showCancel: false,
						success: function(res) {
							if (res.confirm) {
								uni.redirectTo({
									url: '../login/login'
								})
							}
						}
					})
					return
				}
			}
		).catch(
			(response) => { 
				//console.log("response===="+response.statusCode);
				reject(response)
			}
		)
	})
	return promise
};