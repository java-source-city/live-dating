import Vue from 'vue'
import App from './App'
import uview from'uview-ui';
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.use(uview);
Vue.mixin(mpShare);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
